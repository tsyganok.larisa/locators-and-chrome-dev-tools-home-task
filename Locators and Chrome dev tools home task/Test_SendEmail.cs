using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;


namespace WebDriverHomeTask
{
	public class Tests
	{
		readonly String test_url = "https://accounts.ukr.net/";

		IWebDriver driver;

		[SetUp]
		public void start_Browser()
		{
			driver = new ChromeDriver();
			driver.Manage().Window.Maximize();
			driver.Manage().Cookies.DeleteAllCookies();
		}

		[Test]
		public void test_SendEmail()
		{
			driver.Navigate().GoToUrl(test_url);			

			driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

			var loginField = driver.FindElement(By.CssSelector("input[name='login']"));
			loginField.SendKeys("webtester221908@ukr.net");
			var passField = driver.FindElement(By.XPath("//input[@name='password' and @type='password']"));
			passField.SendKeys("xjfyGTY6890LL@");
			var continueButton = driver.FindElement(By.CssSelector("button[type='submit']"));
			continueButton.Click();

			driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
			var verifyEmail = driver.FindElement(By.ClassName("login-button__user"));
			Assert.That(verifyEmail.Text, Is.EqualTo("webtester221908@ukr.net"));

			var composeButton = driver.FindElement(By.XPath("//button[@class='button primary compose']"));
			composeButton.Click();

			var sendToField = driver.FindElement(By.Name("toFieldInput"));
			sendToField.SendKeys("testester190822@gmail.com");

			var subjectField = driver.FindElement(By.Name("subject"));
			subjectField.SendKeys("Test");
						
			var messageField = driver.FindElement(By.Id("mce_0_ifr"));
			messageField.SendKeys("Hello! This is a test. Have a nice day!");

			var sendButton = driver.FindElement(By.ClassName("send"));
			sendButton.Click();
			
		}	
	}
}